###################################################################################################################
Ansicht des Studenten, welcher sich vor Ort befindet und durch Lichtsignale und Handheben mit dem Prof agieren kann.
####################################################################################################################


import cv2
import requests
import time

#Kameraobjekt importieren
cap = cv2.VideoCapture(0)

#QR Code erkennung
detector = cv2.QRCodeDetector()

print("Das Skript wurde gestartet. Sie koenen jetzt Fragen stellen :)")

while True:
    #Kamerabild abrufen
    _, img = cap.read()
    data, bbox, _ = detector.detectAndDecode(img)
    
    #Wenn ein QR Code erkannt wird.
    if(bbox is not None):
        for i in range(len(bbox)):
            cv2.line(img, tuple(bbox[i][0]), tuple(bbox[(i+1) % len(bbox)][0]), color=(255,
                     0, 255), thickness=2)
        cv2.putText(img, data, (int(bbox[0][0][0]), int(bbox[0][0][1]) - 10), cv2.FONT_HERSHEY_SIMPLEX,
                    0.5, (0, 255, 0), 2)
        if data:
            #Sendet Request an IFTT um einen Eintrag in Googlesheets zu erstellen
            r = requests.post(data,timeout = 10)

            if r.status_code == requests.codes.ok:

                print("Request an IFTT gesendet: ", data)
                print("Frage wurde gestellt. Hue Lampe wird rot")
                #Wartet 10 Sekunden, dann wird ein neuer QR Code erkannt
                time.sleep(10)

    cv2.imshow("QR Code erkennen", img)
    #Skript stoppen
    if(cv2.waitKey(1) == ord("q")):
        break

cap.release()
cv2.destroyAllWindows()
